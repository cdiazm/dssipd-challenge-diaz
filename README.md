# Challenge: Choose a Repo of Your Choice on GitHub/GitLab and Run it / Cristóbal Díaz

Hi Orwa!

For this challenge I decided to use this python repository " https://github.com/parulnith/Face-Detection-in-Python-using-OpenCV ", because it was helpful for me when I was studying for my DDP Project.

The python code on this repository it was made for face detection using OpenCV.


## Dependencies
To run this code I followed next steps:

    1- I created an enviroment using Anaconda and I called "face-detection"

![1](./dssipd-challenge-diaz/Challenge_documentation/face-detection_env.png)


    2- In this enviroment I had to install the libraries that I did not have on my computer.


    3- Using pip I installed Opencv with the following command "pip install opencv-contrib-python "

![2](./dssipd-challenge-diaz/Challenge_documentation/install_opencv.png)

    4- and also it was necesarry install Matplotlib with the following command " pip install matplotlib"

![3](./dssipd-challenge-diaz/Challenge_documentation/install_matplotlib.png)

## Running the code
In general terms the code did not have so much issues, only I faced the following comments to run the code whit succes:

    1- I had to put in comments this line "get_ipython().magic('matplotlib inline')", because that command it is for called functions for notebooks code as Jupyter notebook and I wanted to run it only in a pure python enviroment.

![4](./dssipd-challenge-diaz/Challenge_documentation/get_ipython.png)


    2- The extensions of test images that the original code have, dont match with the extensions in the code, thus I had to change it because the code did run it.


    3- the orginal code did not give to me good results, because the bouding boxes covered all the face detection and also did not detect all faces in the image as the following example.

![5](./dssipd-challenge-diaz/Challenge_documentation/result1.png)

    4- To fix that problem I had to study cascade function and all their settings, so whit a few changes in the code, the results was the following.

![6](./dssipd-challenge-diaz/Challenge_documentation/result2.png)

    5- My final test was with differents images from internet to be sure that detection was good and this are the results.

![7](./dssipd-challenge-diaz/Challenge_documentation/result3.png)

![8](./dssipd-challenge-diaz/Challenge_documentation/result4.png)


## Credits

All files used for this university assignment were obtained from a public repository at the following linkhttps://github.com/parulnith/Face-Detection-in-Python-using-OpenCV 